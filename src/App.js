import React from 'react';
import styles from './App.css';
import { Button, List } from 'antd-mobile'
import Dance from './dancing'
import createStore from 'antd/lib/table/createStore';
const store_ = createStore()
// 组件名称必须以大写字母开头
class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      persons: ['演员1', '演员2', '演员3']
    }
  }

  //事件
  addGirl(){
    const store_ = this.props.store;
    const tibu = store_.getState();
    const girls = this.props.girls; 
    store_.dispatch(girls())
    this.setState({ 
      persons: [...this.state.persons, '女演员' + tibu]
    })
  }

  addBoy(){
    const store_ = this.props.store;
    const boys = this.props.boys;
    const tibu = store_.getState();
    store_.dispatch(boys())
    this.setState({ 
      persons: [...this.state.persons, '男演员' + tibu]
    })
  }

  //生命周期  准备加载
  componentWillMount(){
    console.log('组件准备加载')
  }

  //生命周期  加载完毕
  componentDidMount(){
    console.log('组件加载完毕')
  }

  render() {
    const move = '爱情公寓';
    
    return (
      <div className="App">
        <Button type='primary' onClick={() => this.addGirl()}>添加女演员</Button>
        <Button type="primary" onClick={()=>this.addBoy()}>添加男演员</Button>
        <Dance music={move} store_={this.props.store_}></Dance>
        <h1>娱乐节目：{ move }</h1>
        <Zengxiaoxian name = "曾小贤"></Zengxiaoxian>
        <List renderHeader={()=>'演员列表'}>
          {
            this.state.persons.map(
              v => {
                return <List.Item key={v}>{v}</List.Item>
              }
            )
          }
        </List>
      </div>
    );
  }
}

// 函数是定义组件，又称为无状态组件
function Huyifei(props){
  return <h1>{props.name}</h1>
}

//class定义组件,通过this.props访问数据
class Zengxiaoxian extends React.Component{
  render() {
    return (
      <div>
        <Huyifei className={styles.yifei} name = "胡一菲"></Huyifei>
        <h1>{this.props.name}</h1>
      </div>
    )
  }
}

export default App;
