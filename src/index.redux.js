const girl = "女演员：";
const boy = "男演员：";
const music = "相信自己";

// reducer
export function reducer_(state=0, action){
    switch(action.type){
        case girl:
        return state+1
        case boy:
        return state+1
        default:
        return  0
    }
}

//切换music
export function exchangeMusic(state, action){
    switch(action.type){
        case music:
        return music
        default:
        return state
    }
}

// action creator
export function girls(){
    return { type: girl }
}

export function boys(){
    return { type: boy }
}

export function changeMusic_(){
    return { type: music }
}