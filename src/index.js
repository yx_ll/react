import React from 'react';
import { createStore } from 'redux';
import { reducer_, girls, boys, exchangeMusic } from './index.redux'
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//reducer接收两个参数,state,action
const store = createStore(reducer_);
const store_ = createStore(exchangeMusic);
function render(){
    ReactDOM.render(<App store={store} store_={store_} girls={girls} boys={boys}/>, document.getElementById('root'));
}
render()

store.subscribe(render)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
