import React from 'react';
import { Button } from 'antd-mobile';
import { changeMusic_ } from './index.redux'
import createStore from 'antd/lib/table/createStore';
class Dance extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            music: "爱情公寓"
        }
        console.log(this.props.music)
    }
    
    changeMusic (){
        const store_ = this.props.store_;
        store_.dispatch(changeMusic_());
        const music = store_.getState();
        console.log(music)
        this.setState({
            music
        })
    }

    render(){
        return (
            <div>
                <Button type="primary" onClick={()=>this.changeMusic()}>切换主题曲</Button>
                <h1>
                    主题曲：{this.state.music}
                </h1>
            </div>
        )
    }

}

export default Dance